const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { sendEmail } = require("../controllers/sendmail");
require("dotenv/config");
const secretString = process.env.SecretString;

const UserSchema = new Schema({
	name: { type: String, default: "unknown", unique: true },
	email: {
		type: String,
		match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
		unique: true,
	},
	password: { type: String, required: true },
	active: { type: Number, default: 0 },
	blogPosts: { type: mongoose.Schema.Types.ObjectId, ref: "BlogPost" },
	date: { type: Date, default: Date.now() },
});

const User = mongoose.model("User", UserSchema);

// Users
const insertUser = async (name, email, password) => {
	try {
		const encryptedPassword = await bcrypt.hash(password, 10);
		const newUser = new User();
		newUser.name = name;
		newUser.email = email;
		newUser.password = encryptedPassword;
		await newUser.save();
		await sendEmail(email, encryptedPassword);
	} catch (error) {
		throw error;
	}
};

const getUsers = async (query, limit, page) => {
	try {
		const getUser = await User.find({ query }).limit(limit).skip(page);
		return getUser;
	} catch (error) {
		throw error;
	}
};

const activeUser = async (email, key) => {
	try {
		const foundUser = await User.findOne({ email, password: key });
		if (!foundUser) {
			throw `User not found`;
		}
		if (foundUser.active === 0) {
			foundUser.active = 1;
			await foundUser.save();
		} else {
			throw `User is actived`;
		}
	} catch (error) {
		throw error;
	}
};

// Login Function
const loginUser = async (email, password) => {
	try {
		let foundUser = await User.findOne({ email: email.trim() }).exec();
		if (!foundUser) {
			throw "Email or Password is not found";
		} else {
			let encryptedPassword = foundUser.password;
			let checkPassword = await bcrypt.compare(
				password,
				encryptedPassword
			);
			if (!checkPassword) {
				throw "Email or Password is not found";
			} else {
				let jsonObject = {
					id: foundUser._id,
				};
				let tokenKey = await jwt.sign(jsonObject, secretString, {
					expiresIn: 86400,
				});
				return tokenKey;
			}
		}
	} catch (error) {
		throw error;
	}
};

// Verify Token Function
const verifyJWT = async (tokenKey) => {
	try {
		const decodeJwt = await jwt.verify(tokenKey, secretString);
		if (Date.now() / 1000 > decodeJwt.exp) {
			throw "TimeOut";
		}
		let foundUser = await User.findById(decodeJwt.id);
		if (!foundUser) {
			throw "User not exist";
		}
		return foundUser;
	} catch (error) {
		throw error;
	}
};

module.exports = {
	User,
	insertUser,
	activeUser,
	getUsers,
	loginUser,
	verifyJWT,
};
