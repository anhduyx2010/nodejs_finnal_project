const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { verifyJWT } = require("./UserSchema");

const BlogPostSchema = new Schema({
	title: { type: String, default: "", unique: true },
	content: { type: String, default: "" },
	date: { type: Date, default: Date.now },
	author: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
});

const BlogPost = mongoose.model("BlogPost", BlogPostSchema, "blogposts");

const inserBlogPost = async (title, content, tokenKey) => {
	try {
		let signedInUser = await verifyJWT(tokenKey);
		let newBlogPost = await BlogPost.create({
			title,
			content,
			date: Date.now(),
			author: signedInUser,
		});
		await newBlogPost.save();
		return newBlogPost;
	} catch (error) {
		throw error;
	}
};

const findBlogPosts = async (query = null, limit, page) => {
	try {
		//trang 0=> skip = 0*5, limit=5
		//trang 1=> skip = 1*5, limit=5
		//trang 2=> skip = 2*5, limit=5
		//trang n => skip = n*limit limit=5
		const findBlogPosts = BlogPost.find(query).limit(limit).skip(page);
		return findBlogPosts;
	} catch (error) {
		throw error;
	}
};

const findBlogPostById = async (id) => {
	try {
		const findBlogPostById = BlogPost.findById(id);
		return findBlogPostById;
	} catch (error) {
		throw error;
	}
};

const deleteBlogPost = async (blogPostId, tokenKey) => {
	try {
		let signedInUser = await verifyJWT(tokenKey);
		let blogPost = await BlogPost.findById(blogPostId);
		if (!blogPost) {
			throw `Not found with Id=${blogPostId}`;
		}
		if (signedInUser.id !== blogPost.author.toString()) {
			throw "You are not the owner";
		}
		await BlogPost.deleteOne({ _id: blogPostId });
	} catch (error) {
		throw error;
	}
};

const updateBlogPost = async (blogPostId, updatedBlogPost, tokenKey) => {
	try {
		let signedInUser = await verifyJWT(tokenKey);
		let blogPost = await BlogPost.findById(blogPostId);
		if (!blogPost) {
			throw `Blog Post is not exist Id=${blogPostId}`;
		}
		if (signedInUser.id !== blogPost.author.toString()) {
			throw "You are not the owner";
		}
		blogPost.title = !updatedBlogPost.title
			? blogPost.title
			: updatedBlogPost.title;
		blogPost.content = !updatedBlogPost.content
			? blogPost.content
			: updatedBlogPost.content;
		blogPost.date = Date.now();
		await blogPost.save();
		return blogPost;
	} catch (error) {
		throw error;
	}
};

module.exports = {
	BlogPost,
	inserBlogPost,
	findBlogPosts,
	findBlogPostById,
	deleteBlogPost,
	updateBlogPost,
};
