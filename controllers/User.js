const {insertUser, activeUser, getUsers, loginUser, verifyJWT} = require('../models/UserSchema');

exports.createUser = async (req,res) => {
    const {name, email, password} = req.body;

    try {
        await insertUser( name, email,  password);
        res.json({
            result: "Oke",
            message: "User Create Success"
        })
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error : ${error}`
        })
    }
}

exports.activeUser = async(req,res) => {
    let {email, key} = req.query
    try {
        await activeUser(email, key);
        res.send(`<h1 style="sea: sea">User is active</h1>`)
    } catch (error) {
        res.send(`<h1 style="color: sea">User already activated</h1>`)
    }
}

exports.getUsers = async (req,res) => {
    const {query,limit,page} = req.query;
    try {
        const findUser = await getUsers(query, limit, page)
        res.send(findUser)
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error : ${error}`
        })
    }
}

exports.loginUser = async(req,res) => {
    let {email, password} = req.body
    try {
        let tokenKey = await loginUser(email, password);
        console.log(tokenKey)
        res.json({
            result: 'Oke',
            message: 'Login Oke',
            tokenKey: tokenKey
        })
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error : ${error}`
        })
    }
};

exports.checkToken = async (req,res) => {
    let tokenKey = req.headers['x-access-token']
    try {
        await verifyJWT(tokenKey)
        res.json({
			result: 'Ok',
            message: 'Verify Json Web Token Success',
	  	})	
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error : ${error}`
        })
    }
}