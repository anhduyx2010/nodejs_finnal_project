const { BlogPost, inserBlogPost, findBlogPosts, findBlogPostById, deleteBlogPost, updateBlogPost } = require('../models/BlogPostSchema');

exports.createBlogPost = async (req, res) => {
    let { title, content } = req.body;
    let tokenKey = req.headers['x-access-token']

    try {
        const newBlogPost = await inserBlogPost(title, content, tokenKey)
        res.json({
            result: "Oke",
            message: "Create new BlogPost done!",
            data: newBlogPost
        });
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error: ${error}`
        })
    }
}
exports.getBlogPost = async(req,res) => {
    let {query, limit, page} = req.query;
    try {
        const getBlogPost = await findBlogPosts(query, limit, page)
        res.json({
            result: "Oke",
            message: "Get BlogPosts Done!",
            data: getBlogPost
        })
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error: ${error}`
        })
    }
}

exports.getBlogPostById = async(req,res) => {
    let {id} = req.params;
    try {
        const getBlogPostById = await findBlogPostById(id);
        res.json({
            result: "Oke",
            message: "Get BlogPosts Done!",
            data: getBlogPostById
        })
    } catch (error) {
        res.json({
            result: 'Failed',
            message: `Error: ${error}`
        })
    }
}

exports.deleteBlogPost = async(req,res) => {
    let {id} = req.params	
	let tokenKey = req.headers['x-access-token']	
    try {    	
        await deleteBlogPost(id, tokenKey)
        res.json({
            result: 'Ok',
            message: 'Blog Post is deleted',	  		
        })	
	} catch(error) {
		res.json({
            result: 'Failed',
            message: `Error: ${error}`
        })
	}
}

exports.updateBlogPost = async(req,res) => {
    let {id} = req.params;
    let updatedBlogPost = req.body;
    let tokenKey = req.headers['x-access-token'];
    try {    	
    	let blogPost = await updateBlogPost(id, updatedBlogPost,tokenKey);
        res.json({
            result: 'Ok',
            message: 'Update success BlogPost',
            data: blogPost
        });
    } catch(error) {
		res.json({
            result: 'Failed',
            message: `Error: ${error}`
        })
	}
}