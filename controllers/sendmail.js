const nodemailer = require('nodemailer');


const PORT = 3000
const sendEmail = async (receiverEmail, key) => {	    
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: "nh0xb0ykut3lk42@gmail.com", 
                pass: "Anhduy123"
            }
        })
        let mailOptions = {
            from: "nh0xb0ykut3lk42@gmail.com", 
            to: receiverEmail, 
            subject: 'Activate User',         
            html: `<h1>Please click here to activate your account:</h1>
                   http://${require('os').hostname()}:${PORT}/api/activate?key=${key}&email=${receiverEmail}` 
        }
        let info = await transporter.sendMail(mailOptions)
        console.log('Message sent: %s', info.messageId);
    } catch(error) {
        throw error
    }
}
module.exports = {
    sendEmail, 
    PORT   
}