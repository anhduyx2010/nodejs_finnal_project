var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv/config");

var apiRouter = require("./routes/api");
const apiBlogPost = require("./routes/blogpost");

var app = express();

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept"
	);
	next();
});

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api", apiRouter);
app.use("/blog", apiBlogPost);

// Connect Mongo DB
mongoose.connect(process.env.DB_Url, { useNewUrlParser: true }, () =>
	console.log("Connected DB")
);
module.exports = app;
