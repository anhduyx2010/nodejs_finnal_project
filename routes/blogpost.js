var express = require('express');
var router = express.Router();
const {createBlogPost, getBlogPost, getBlogPostById, deleteBlogPost, updateBlogPost } = require('../controllers/BlogPost');

router.post('/create', createBlogPost);
router.get('/', getBlogPost);
router.get('/:id', getBlogPostById);
router.delete('/:id', deleteBlogPost);
router.put('/:id', updateBlogPost)
module.exports = router;