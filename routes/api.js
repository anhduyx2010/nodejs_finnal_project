var express = require("express");
var router = express.Router();
const {
	createUser,
	activeUser,
	getUsers,
	loginUser,
	checkToken,
} = require("../controllers/User");

// Users
router.get("/", getUsers);
router.post("/", createUser);
router.get("/activate", activeUser);

// Login
router.post("/login", loginUser);
router.get("/token-jwt", checkToken);

module.exports = router;
